package kyle.shiftaway;

import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class SettingsMySchedule extends AppCompatActivity {

    scheduledatabase myDb;
    ListView dbList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_my_schedule);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        myDb = new scheduledatabase(this);
        dbList = (ListView)findViewById(R.id.schedule_list);

        populateListView();
        dbList.invalidateViews();
    }

    private void populateListView() {
        Cursor cursor = myDb.getMyData();
        String[] from = new String[]{"NAME", "DEPARTMENT", "DATE", "TIME"};
        int[] to = new int[]{R.id.rowName, R.id.rowDepartment, R.id.rowDate, R.id.rowTime};
        final SimpleCursorAdapter myCursorAdapter = new SimpleCursorAdapter(getBaseContext(), R.layout.listviewlayout, cursor, from, to, 0);
        dbList.setAdapter(myCursorAdapter);
    }

}
