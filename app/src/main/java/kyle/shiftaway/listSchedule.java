package kyle.shiftaway;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class listSchedule extends AppCompatActivity{

    scheduledatabase myDb;
    ListView dbList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_schedule);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        myDb = new scheduledatabase(this);
        dbList = (ListView)findViewById(R.id.schedule_list);

        populateListView();
        dbList.invalidateViews();

        //This section is used to make it so clicking on each schedule in the list will pop up with
        //a confirmation window and allow you to accept the shift.
        dbList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, final long id) {
                Cursor curs = myDb.getData(id);
                String department = "";
                String date = "";
                String hours = "";
                if (curs.moveToFirst()) {
                    department = curs.getString(curs.getColumnIndex("DEPARTMENT"));
                    date = curs.getString(curs.getColumnIndex("DATE"));
                    hours = curs.getString(curs.getColumnIndex("TIME"));
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(listSchedule.this);
                String newLine = System.getProperty("line.separator");
                builder.setMessage("Take this shift? " + newLine + newLine + department + " " + date + " " + hours);

                //This sets ups the confirmation button and exit button.
                builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        myDb.deleteIndividual(id);
                        dbList.invalidateViews();
                        Intent intent = new Intent(listSchedule.this, listSchedule.class);
                        listSchedule.this.startActivity(intent);
                        finish();
                    }
                });
                builder.create();
                builder.show();
            }
        });

    }

    public void scheduleEdit(View view){
        Intent intent = new Intent(this, scheduleadd.class);
        startActivity(intent);
    }

    /*
    Populates the list view with information from the database
     */
    private void populateListView() {
        Cursor cursor = myDb.getAllData();
        String[] from = new String[]{"NAME", "DEPARTMENT", "DATE", "TIME"};
        int[] to = new int[]{R.id.rowName, R.id.rowDepartment, R.id.rowDate, R.id.rowTime};
        final SimpleCursorAdapter myCursorAdapter = new SimpleCursorAdapter(getBaseContext(), R.layout.listviewlayout, cursor, from, to, 0);
        dbList.setAdapter(myCursorAdapter);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), HomePageLoggedIn.class);
        startActivityForResult(myIntent, 0);
        return true;

    }

}
