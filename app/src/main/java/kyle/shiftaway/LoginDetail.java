package kyle.shiftaway;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginDetail extends AppCompatActivity implements View.OnClickListener {

    Button logout;
    EditText etName, etEmail;
    UserLocalStore userLocalStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_detail);

        etName = (EditText) findViewById(R.id.etUsername);
        etEmail = (EditText) findViewById(R.id.etEmail);

        logout = (Button) findViewById(R.id.logout);

        logout.setOnClickListener(this);

        userLocalStore = new UserLocalStore(this);
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (authenticate() == true){
            displayUserDetails();
        }

    }

    private boolean authenticate(){
        return userLocalStore.getUerLoggedIn();
    }

    private void displayUserDetails(){
        User user = userLocalStore.getLoggedInUser();

        etName.setText(user.name);
        etEmail.setText(user.email);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.logout:

                userLocalStore.clearUserData();
                userLocalStore.setUserLoggedIn(false);

                startActivity(new Intent(this, LoginActivity.class));

                break;
        }
    }
}

