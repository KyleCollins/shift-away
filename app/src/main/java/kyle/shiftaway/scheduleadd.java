package kyle.shiftaway;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class scheduleadd extends AppCompatActivity {

    scheduledatabase myDb;
    EditText editName, editDepartment, editDate, editHours;
    Button btnAdd;
    Button btnViewAll;

    public static final int NOTIFICATION_ID = 1;


    public void sendNotification(View view) {

        // Use NotificationCompat.Builder to set up our notification.
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        //icon appears in device notification bar and right hand corner of notification
        builder.setSmallIcon(R.drawable.download);

        // This intent is fired when notification is clicked
        Intent intent = new Intent(this, listSchedule.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        // Set the intent that will fire when the user taps the notification.
        builder.setContentIntent(pendingIntent);

        // Content title, which appears in large type at the top of the notification
        builder.setContentTitle("ShiftAway Shift!");

        // Content text, which appears in smaller text below the title
        builder.setContentText("The Following shift has been added to the list of schedules!");

        // The subtext, which appears under the text on newer devices.
        // This will show-up in the devices with Android 4.2 and above only
        builder.setSubText("Tap to open the schedule list");

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Will display the notification in the notification bar
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    // Sets an ID for the notification
    int mNotificationId = 002;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scheduleadd);

        myDb = new scheduledatabase(this);

        editName = (EditText)findViewById(R.id.nameEdit);
        editDepartment = (EditText)findViewById(R.id.departmentEdit);
        editDate = (EditText)findViewById(R.id.dateEdit);
        editHours = (EditText)findViewById(R.id.hoursEdit);
        btnAdd = (Button)findViewById(R.id.button_add);
        btnViewAll = (Button)findViewById(R.id.button_View);
        AddData();
        viewAll();
    }

    public void AddData(){
        btnAdd.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View V) {
                        boolean isInserted = myDb.insertData(editName.getText().toString(), editDepartment.getText().toString(), editDate.getText().toString(), editHours.getText().toString());

                        if (isInserted = true) {
                            sendNotification(V);
                            Intent intent = new Intent(scheduleadd.this, listSchedule.class);
                            scheduleadd.this.startActivity(intent);
                        }
                        else
                            Toast.makeText(scheduleadd.this, "Error with adding", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    public void viewAll(){
        btnViewAll.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View v){
                        myDb.removeAll();
                    }
                }
        );
    }

    public void showMessage(String title, String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }

    public void goBack(View view){
        Intent intent = new Intent(this, listSchedule.class);
        startActivity(intent);
    }

}
