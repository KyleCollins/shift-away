package kyle.shiftaway;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class HomePageLoggedIn extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page_logged_in);

    }


    public void logout(View view){
        Intent intent = new Intent(this, Homepage.class);
        startActivity(intent);
    }

    public void schedule(View view){
        Intent intent = new Intent(this, listSchedule.class);
        startActivity(intent);
    }

    public void workplace(View view){
        Intent intent = new Intent(this, workplace.class);
        startActivity(intent);
    }

    public void settings(View view){
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

}
