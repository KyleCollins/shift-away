package kyle.shiftaway;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by KyleC on 4/5/2016.
 */
public class scheduledatabase extends SQLiteOpenHelper{
    public static final String DATABASE_NAME="schedule.db";
    public static final String TABLE_NAME="schedule_table";
    public static final String COL_1="NAME";
    public static final String COL_2="DEPARTMENT";
    public static final String COL_3="DATE";
    public static final String COL_4="TIME";
    public static final String COL_5="TAKEN";

    public scheduledatabase(Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME + " (NAME TEXT,DEPARTMENT TEXT,DATE TEXT,TIME TEXT, TAKEN INT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(String name, String department, String date, String time) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1, name);
        contentValues.put(COL_2, department);
        contentValues.put(COL_3, date);
        contentValues.put(COL_4, time);
        long result = db.insert(TABLE_NAME, null, contentValues);
        if(result == -1)
            return false;
        else
            return true;
    }

    public Cursor getAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select rowid _id,* from " + TABLE_NAME + " WHERE NAME != 'x'", null);
        return res;
    }

    public Cursor addColumn(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("ALTER TABLE schedule_table ADD COLUMN TAKEN INT", null);
        return res;
    }

    public Cursor getData(long id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE rowid = " + id, null);
        return res;
    }

    public Cursor getMyData(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT rowid _id,* FROM " + TABLE_NAME + " WHERE NAME = 'x'", null);
        return res;
    }

    /*
    This is used to replace the name of an individual with the id that is passed into the parameter
    The name is replaces with whatever value the values.put has in it.
     */
    public boolean deleteIndividual(long id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor curs = getData(id);
        String name = "";
        if(curs.moveToFirst()){
            name = curs.getString(curs.getColumnIndex("NAME"));
        }


        ContentValues values = new ContentValues();
        values.put(COL_1, "x");


        db.update(scheduledatabase.TABLE_NAME,values, "NAME = ?",new String[]{name});
        return true;

        //db.delete(scheduledatabase.TABLE_NAME,"rowid=" + String.valueOf(id) ,null);
    }

    public scheduledatabase open(){
        SQLiteDatabase db = this.getWritableDatabase();
        return this;
    }

    public void removeAll(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(scheduledatabase.TABLE_NAME,null,null);
    }
}
