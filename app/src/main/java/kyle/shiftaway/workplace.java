package kyle.shiftaway;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class workplace extends AppCompatActivity {

    scheduledatabase myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workplace);

        myDb = new scheduledatabase(this);
    }

    public void addWorkPlace(View view){
        Intent intent = new Intent(this, workPlaceAdd.class);
        startActivity(intent);
    }
}
